package nl.bitart.unfairdistricts;


import nl.bitart.unfairdistricts.dto.Score;
import nl.bitart.unfairdistricts.dto.Solution;
import nl.bitart.unfairdistricts.dto.VoteCell;
import nl.bitart.unfairdistricts.core.Grid;
import nl.bitart.unfairdistricts.main.Kata;
import nl.bitart.unfairdistricts.main.KataInput;
import nl.bitart.unfairdistricts.utils.GridUtils;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class KataTest {

    private final Kata fixture = new Kata();


    @Test
    void basic6() {

        Grid inputGrid = new Grid(2, 3);

        inputGrid.setVoteCell(new VoteCell(2, 1, 0, 0, "A"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 0, 1, "B"));
        inputGrid.setVoteCell(new VoteCell(1, 2, 0, 2, "C"));
        inputGrid.setVoteCell(new VoteCell(2, 1, 1, 0, "D"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 1, 1, "E"));
        inputGrid.setVoteCell(new VoteCell(0, 2, 1, 2, "F"));

        KataInput input = new KataInput(inputGrid, 5);

        List<Solution> solutions = fixture.solveKata(input);
        assertThat(solutions.size()).isEqualTo(1);
        Map<Score, Solution> solutionsMap = getSolutionsMap(inputGrid, solutions);
        for (Map.Entry<Score, Solution> entry : solutionsMap.entrySet()) {
            printSolution(entry);
        }
    }


    @Test
    void basic9() {

        Grid inputGrid = new Grid(3, 3);

        inputGrid.setVoteCell(new VoteCell(0, 3, 0, 0, "A"));
        inputGrid.setVoteCell(new VoteCell(3, 3, 0, 1, "B"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 0, 2, "C"));
        inputGrid.setVoteCell(new VoteCell(1, 2, 1, 0, "D"));
        inputGrid.setVoteCell(new VoteCell(1, 0, 1, 1, "E"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 1, 2, "F"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 2, 0, "G"));
        inputGrid.setVoteCell(new VoteCell(2, 1, 2, 1, "H"));
        inputGrid.setVoteCell(new VoteCell(2, 2, 2, 2, "I"));

        KataInput input = new KataInput(inputGrid, 9);
        List<Solution> solutions = fixture.solveKata(input);
        assertThat(solutions.size()).isEqualTo(2);
        Map<Score, Solution> solutionsMap = getSolutionsMap(inputGrid, solutions);
        for (Map.Entry<Score, Solution> entry : solutionsMap.entrySet()) {
            printSolution(entry);
        }
    }

    @Test
    void basic16() {

        Grid inputGrid = new Grid(4, 4);

        inputGrid.setVoteCell(new VoteCell(1, 1, 0, 0, "A"));
        inputGrid.setVoteCell(new VoteCell(2, 0, 0, 1, "B"));
        inputGrid.setVoteCell(new VoteCell(2, 0, 0, 2, "C"));
        inputGrid.setVoteCell(new VoteCell(3, 3, 0, 3, "D"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 1, 0, "E"));
        inputGrid.setVoteCell(new VoteCell(1, 2, 1, 1, "F"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 1, 2, "G"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 1, 3, "H"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 2, 0, "I"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 2, 1, "J"));
        inputGrid.setVoteCell(new VoteCell(1, 2, 2, 2, "K"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 2, 3, "L"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 3, 0, "M"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 3, 1, "N"));
        inputGrid.setVoteCell(new VoteCell(1, 1, 3, 2, "O"));
        inputGrid.setVoteCell(new VoteCell(2, 0, 3, 3, "P"));

        KataInput input = new KataInput(inputGrid, 8);
        List<Solution> solutions = fixture.solveKata(input);
        assertThat(solutions.size()).isEqualTo(4);
        Map<Score, Solution> solutionsMap = getSolutionsMap(inputGrid, solutions);
        for (Map.Entry<Score, Solution> entry : solutionsMap.entrySet()) {
            printSolution(entry);
        }
    }

    @Test
    void basic25() {

        Grid inputGrid = new Grid(5, 5);

        inputGrid.setVoteCell(new VoteCell(1, 0, 0, 0, "A"));
        inputGrid.setVoteCell(new VoteCell(0, 5, 0, 1, "B"));
        inputGrid.setVoteCell(new VoteCell(0, 1, 0, 2, "C"));
        inputGrid.setVoteCell(new VoteCell(5, 0, 0, 3, "D"));
        inputGrid.setVoteCell(new VoteCell(1, 0, 0, 4, "E"));
        inputGrid.setVoteCell(new VoteCell(0, 2, 1, 0, "F"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 1, 1, "G"));
        inputGrid.setVoteCell(new VoteCell(0, 2, 1, 2, "H"));
        inputGrid.setVoteCell(new VoteCell(0, 4, 1, 3, "I"));
        inputGrid.setVoteCell(new VoteCell(2, 0, 1, 4, "J"));
        inputGrid.setVoteCell(new VoteCell(3, 0, 2, 0, "K"));
        inputGrid.setVoteCell(new VoteCell(4, 0, 2, 1, "L"));
        inputGrid.setVoteCell(new VoteCell(1, 0, 2, 2, "M"));
        inputGrid.setVoteCell(new VoteCell(0, 5, 2, 3, "N"));
        inputGrid.setVoteCell(new VoteCell(0, 4, 2, 4, "O"));
        inputGrid.setVoteCell(new VoteCell(0, 5, 3, 0, "P"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 3, 1, "Q"));
        inputGrid.setVoteCell(new VoteCell(2, 0, 3, 2, "R"));
        inputGrid.setVoteCell(new VoteCell(5, 0, 3, 3, "S"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 3, 4, "T"));
        inputGrid.setVoteCell(new VoteCell(0, 4, 4, 0, "U"));
        inputGrid.setVoteCell(new VoteCell(0, 1, 4, 1, "V"));
        inputGrid.setVoteCell(new VoteCell(0, 2, 4, 2, "W"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 4, 3, "X"));
        inputGrid.setVoteCell(new VoteCell(0, 4, 4, 4, "Y"));

        KataInput input = new KataInput(inputGrid, 15);
        List<Solution> solutions = fixture.solveKata(input);
        //assertThat(solutions.size()).isEqualTo(87);

        Map<Score, Solution> solutionsMap = getSolutionsMap(inputGrid, solutions);
        for (Map.Entry<Score, Solution> entry : solutionsMap.entrySet()) {
            printSolution(entry);
        }
    }

    private Map<Score, Solution> getSolutionsMap(Grid grid, List<Solution> solutions) {
        Map<Score, Solution> solutionsMap = new TreeMap<>();

        for (Solution solution : solutions) {
            Score score = GridUtils.getScore(grid, solution);
            solutionsMap.put(score, solution);
        }
        return solutionsMap;
    }


    private void printSolution(Map.Entry<Score, Solution> entry) {
        System.out.println(entry.getValue().toString());
        System.out.println(entry.getKey().toString());
        System.out.println("--------------------------------");
    }
}

