package nl.bitart.unfairdistricts.utils;


import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;


public class BoundaryUtilsTest {


    @Test
    void getMaximumDistrictSizeGrid9() {

        List<Integer> unitSizes = Arrays.asList(1, 2, 2, 3, 3, 3, 3, 4, 6);

        int max = BoundaryUtils.getMaximumDistrictSize(8, unitSizes);
        assertThat(max).isEqualTo(4);
    }


    @Test
    void getMaximumDistrictSizeGrid16() {

        List<Integer> unitSizes = Arrays.asList(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 6);

        int max = BoundaryUtils.getMaximumDistrictSize(8, unitSizes);
        assertThat(max).isEqualTo(4);
    }


    @Test
    void getMinimumSolutionSizeGrid9() {

        Set<Integer> keyset = new HashSet<>(Arrays.asList(2, 3, 4));
        int min = BoundaryUtils.getMinimumSolutionSize(9, keyset);
        assertThat(min).isEqualTo(3);
    }

    @Test
    void getMaximumSolutionSizeGrid9() {

        Set<Integer> keyset = new HashSet<>(Arrays.asList(2, 3, 4));
        int max = BoundaryUtils.getMaximumSolutionSize(9, 2, keyset);
        assertThat(max).isEqualTo(4);
    }

    @Test
    void getMaximumSolutionSizeGrid16() {

        Set<Integer> keyset = new HashSet<>(Arrays.asList(2, 3, 4));
        int max = BoundaryUtils.getMaximumSolutionSize(16, 4, keyset);
        assertThat(max).isEqualTo(8);
    }

}
