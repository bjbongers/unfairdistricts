package nl.bitart.unfairdistricts.utils;


import nl.bitart.unfairdistricts.core.Factor;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class FactorUtilTest {

    @Test
    void divideFactors() {

        Set<Factor> factors = FactorUtils.divideFactors(8, 2, new HashSet(Arrays.asList(2, 3, 4, 5)));
        for (Factor factor : factors) {
            System.out.println(factor);
        }

        //assertThat(factors.size()).isEqualTo(2);

    }

}
