package nl.bitart.unfairdistricts.utils;


import nl.bitart.unfairdistricts.core.Grid;
import nl.bitart.unfairdistricts.dto.District;
import nl.bitart.unfairdistricts.dto.VoteCell;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class GridUtilsTest {

    @Test
    void getMaximumSolutionSizeGrid9() {

        Grid grid = new Grid(4, 4);

        grid.setVoteCell(new VoteCell(1, 1, 0, 0, "A"));
        grid.setVoteCell(new VoteCell(2, 0, 0, 1, "B"));
        grid.setVoteCell(new VoteCell(2, 0, 0, 2, "C"));
        grid.setVoteCell(new VoteCell(3, 3, 0, 3, "D"));
        grid.setVoteCell(new VoteCell(1, 1, 1, 0, "E"));
        grid.setVoteCell(new VoteCell(1, 2, 1, 1, "F"));
        grid.setVoteCell(new VoteCell(1, 1, 1, 2, "G"));
        grid.setVoteCell(new VoteCell(0, 3, 1, 3, "H"));
        grid.setVoteCell(new VoteCell(1, 1, 2, 0, "I"));
        grid.setVoteCell(new VoteCell(1, 1, 2, 1, "J"));
        grid.setVoteCell(new VoteCell(1, 2, 2, 2, "K"));
        grid.setVoteCell(new VoteCell(0, 3, 2, 3, "L"));
        grid.setVoteCell(new VoteCell(1, 1, 3, 0, "M"));
        grid.setVoteCell(new VoteCell(1, 1, 3, 1, "N"));
        grid.setVoteCell(new VoteCell(1, 1, 3, 2, "O"));
        grid.setVoteCell(new VoteCell(2, 0, 3, 3, "P"));

        District d1 = new District();
        d1.combine("C");
        d1.combine("F");
        d1.combine("J");
        d1.combine("H");

        assertThat(GridUtils.isAdjacentDistrict(grid, d1)).isFalse();
    }


    @Test
    void isAdjacent() {

        Grid inputGrid = new Grid(5, 5);

        inputGrid.setVoteCell(new VoteCell(1, 0, 0, 0, "A"));
        inputGrid.setVoteCell(new VoteCell(0, 5, 0, 1, "B"));
        inputGrid.setVoteCell(new VoteCell(0, 1, 0, 2, "C"));
        inputGrid.setVoteCell(new VoteCell(5, 0, 0, 3, "D"));
        inputGrid.setVoteCell(new VoteCell(1, 0, 0, 4, "E"));
        inputGrid.setVoteCell(new VoteCell(0, 2, 1, 0, "F"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 1, 1, "G"));
        inputGrid.setVoteCell(new VoteCell(0, 2, 1, 2, "H"));
        inputGrid.setVoteCell(new VoteCell(0, 4, 1, 3, "I"));
        inputGrid.setVoteCell(new VoteCell(2, 0, 1, 4, "J"));
        inputGrid.setVoteCell(new VoteCell(3, 0, 2, 0, "K"));
        inputGrid.setVoteCell(new VoteCell(4, 0, 2, 1, "L"));
        inputGrid.setVoteCell(new VoteCell(1, 0, 2, 2, "M"));
        inputGrid.setVoteCell(new VoteCell(0, 5, 2, 3, "N"));
        inputGrid.setVoteCell(new VoteCell(0, 4, 2, 4, "O"));
        inputGrid.setVoteCell(new VoteCell(0, 5, 3, 0, "P"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 3, 1, "Q"));
        inputGrid.setVoteCell(new VoteCell(2, 0, 3, 2, "R"));
        inputGrid.setVoteCell(new VoteCell(5, 0, 3, 3, "S"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 3, 4, "T"));
        inputGrid.setVoteCell(new VoteCell(0, 4, 4, 0, "U"));
        inputGrid.setVoteCell(new VoteCell(0, 1, 4, 1, "V"));
        inputGrid.setVoteCell(new VoteCell(0, 2, 4, 2, "W"));
        inputGrid.setVoteCell(new VoteCell(0, 3, 4, 3, "X"));
        inputGrid.setVoteCell(new VoteCell(0, 4, 4, 4, "Y"));

        District d1 = new District();
        d1.combine("A");
        d1.combine("B");
        d1.combine("F");
        d1.combine("K");
        d1.combine("L");

        boolean  isAdjacent = GridUtils.isAdjacentDistrict(inputGrid, d1);
        assertThat(isAdjacent).isTrue();

        District d2 = new District();
        d2.combine("U");
        d2.combine("V");
        d2.combine("Q");
        d2.combine("X");
        d2.combine("Y");

        isAdjacent = GridUtils.isAdjacentDistrict(inputGrid, d2);
        assertThat(isAdjacent).isFalse();
    }



}
