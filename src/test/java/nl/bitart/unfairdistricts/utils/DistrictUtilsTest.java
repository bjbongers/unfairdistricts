package nl.bitart.unfairdistricts.utils;


import nl.bitart.unfairdistricts.dto.District;
import nl.bitart.unfairdistricts.dto.Solution;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


public class DistrictUtilsTest {

    @Test
    public void crossProduct1() {

        Set<String> set1 = new TreeSet<>();
        set1.add("A");
        set1.add("B");

        Set<String> set2 = new TreeSet<>();
        set2.add("A");
        set2.add("B");
        set2.add("C");

        Set<String> set3 = new TreeSet<>();
        set3.add("A");
        set3.add("B");

        List<Set<String>> multiSet = new ArrayList<>();
        multiSet.add(set1);
        multiSet.add(set2);
        multiSet.add(set3);

        Set<District> combs = DistrictUtils.crossProductVoteCell(multiSet);
        for (District elm : combs) {
            System.out.println(elm);
        }

        assertThat(combs.size()).isEqualTo(1);
        District first = combs.iterator().next();
        assertThat(first.getName()).isEqualTo("ABC");
    }


    @Test
    public void crossProduct2() {

        Set<String> set1 = new TreeSet<>();
        set1.add("A");
        set1.add("B");
        set1.add("C");

        Set<String> set2 = new TreeSet<>();
        set2.add("A");
        set2.add("B");
        set2.add("C");

        Set<String> set3 = new TreeSet<>();
        set3.add("A");
        set3.add("B");
        set3.add("C");

        List<Set<String>> multiSet = new ArrayList<>();
        multiSet.add(set1);
        multiSet.add(set2);
        multiSet.add(set3);

        Set<District> combs = DistrictUtils.crossProductVoteCell(multiSet);
        for (District elm : combs) {
            System.out.println(elm);
        }

        assertThat(combs.size()).isEqualTo(1);
        District first = combs.iterator().next();
        assertThat(first.getName()).isEqualTo("ABC");
    }

    @Test
    public void crossProductSolution1() {

        Set<District> set1 = new TreeSet<>();
        set1.add(createDistrict("F", "I", "H"));
        set1.add(createDistrict("A", "D", "G"));

        Set<District> set2 = new TreeSet<>();
        set2.add(createDistrict("A", "B"));

        List<Set<District>> multiSet = new ArrayList<>();
        multiSet.add(set1);
        multiSet.add(set2);

        Set<Solution> combs = DistrictUtils.crossProductDistrict(multiSet);

        for (Solution elm : combs) {
            System.out.println(elm);
        }

        assertThat(combs.size()).isEqualTo(1);
        Iterator<Solution> iter = combs.iterator();
        Solution first = iter.next();
        assertThat(first.getName()).isEqualTo("AB;FHI");
    }

    @Test
    public void crossProductSolution2() {

        Set<District> set1 = new TreeSet<>();
        set1.add(createDistrict("F", "I", "H"));
        set1.add(createDistrict("A", "D", "G"));

        Set<District> set2 = new TreeSet<>();
        set2.add(createDistrict("F", "I", "H"));
        set2.add(createDistrict("A", "D", "G"));

        List<Set<District>> multiSet = new ArrayList<>();
        multiSet.add(set1);
        multiSet.add(set2);

        Set<Solution> combs = DistrictUtils.crossProductDistrict(multiSet);

        for (Solution elm : combs) {
            System.out.println(elm);
        }

        assertThat(combs.size()).isEqualTo(1);
        Iterator<Solution> iter = combs.iterator();
        Solution first = iter.next();
        assertThat(first.getName()).isEqualTo("ADG;FHI");
    }

    private District createDistrict(String... elements) {
        District result = new District();

        for (String elm : elements) {
            result.combine(elm);
        }
        return result;
    }
}
