package nl.bitart.unfairdistricts.dto;

import java.util.Set;
import java.util.TreeSet;


public class District implements Comparable {

    private final Set<String> children;

    public District() {
        this.children = new TreeSet<>();
    }

    public District(District clone) {
        this.children = new TreeSet<>();
        children.addAll(clone.getChildren());
    }

    public void combine(String e) {
        children.add(e);
    }

    public Set<String> getChildren() {
        return children;
    }

    public String getName() {

        String name = "";
        for( String d : getChildren()) {
            name = name + d;
        }
        return name;
    }

    public String toString() {
        return getName();
    }

    @Override
    public int compareTo(Object o) {
        return this.toString().compareTo(o.toString());
    }

    public boolean contains(String elm) {
        return this.getChildren().contains(elm);
    }



}