package nl.bitart.unfairdistricts.dto;


import java.util.Set;
import java.util.TreeSet;


public class Solution implements Comparable {

    private Set<District> children;

    // constructor
    public Solution() {
        this.children = new TreeSet<>();
    }

    public Solution(Solution clone) {
        this.children = new TreeSet<>();
        this.children.addAll(clone.getChildren());
    }

    public void combine(District e) {
        children.add(e);
    }

    public Set<District> getChildren() {
        return children;
    }

    public String getName() {

        String name = "";
        int k = 0;
        for (District d : getChildren()) {
            if (k > 0) {
                name = name + ";";
            }
            name = name + d.getName();
            k++;
        }
        return name;
    }

    public String toString() {
        return getName();
    }

    @Override
    public int compareTo(Object o) {
        return this.toString().compareTo(o.toString());
    }

}
