package nl.bitart.unfairdistricts.dto;


public class Score implements Comparable<Score> {

    private final Integer wins;
    private final Integer losses;
    private final Integer draws;

    public Score(Integer wins, Integer losses, Integer draws) {
        this.wins = wins;
        this.losses = losses;
        this.draws = draws;
    }

    public String toString() {
        return "Wins: " + wins + " losses: " + losses + " draws: " + draws;
    }

    @Override
    public int compareTo(Score score) {

        if (wins.equals(score.wins)) {

            if (losses.equals(score.losses)) {
                return 1;
            } else {
                return score.losses.compareTo(this.losses);
            }
        } else {
            return score.wins.compareTo(this.wins);
        }
    }
}
