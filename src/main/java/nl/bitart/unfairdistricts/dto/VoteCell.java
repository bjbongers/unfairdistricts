package nl.bitart.unfairdistricts.dto;


public class VoteCell implements Comparable {

    private final int    voteA;
    private final int    voteB;
    private final int    row;
    private final int    column;
    private final String description;
    private       String district;

    public VoteCell(int voteA, int voteB, int row, int column, String description) {
        this.voteA = voteA;
        this.voteB = voteB;
        this.row = row;
        this.column = column;
        this.description = description;
    }

    public VoteCell(String description) {
        this.voteA = 0;
        this.voteB = 0;
        this.row = 0;
        this.column = 0;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public int getVoteA() {
        return voteA;
    }

    public int getVoteB() {
        return voteB;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public String toString() {
        return getDescription();
    }

    @Override
    public int compareTo(Object o) {
        return this.toString().compareTo(o.toString());
    }
}
