package nl.bitart.unfairdistricts.main;


import nl.bitart.unfairdistricts.core.Factor;
import nl.bitart.unfairdistricts.core.Grid;
import nl.bitart.unfairdistricts.dto.District;
import nl.bitart.unfairdistricts.dto.Solution;
import nl.bitart.unfairdistricts.utils.BoundaryUtils;
import nl.bitart.unfairdistricts.utils.DistrictUtils;
import nl.bitart.unfairdistricts.utils.FactorUtils;
import nl.bitart.unfairdistricts.utils.GridUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class Kata {

    public List<Solution> solveKata(KataInput input) {

        Grid grid = input.getGrid();
        Map<Integer, Set<String>> peoplePerVoteCell = GridUtils.getPeoplePerVoteCell(grid);

        for (Map.Entry<Integer, Set<String>> entry : peoplePerVoteCell.entrySet()) {
            System.out.println("peoplePerVoteCell : " + entry.getKey() + " - " + entry.getValue());
        }

        List<Integer> unitSizes = getPossibleUnitSizes(peoplePerVoteCell);

        int maxDistrictSize = BoundaryUtils.getMaximumDistrictSize(input.getPeoplePerDistrict(), unitSizes);
        int minDistrictSize = 1;
        //int maxDistrictSize = 8;

        Map<Integer, Set<District>> districtsMap = new HashMap<>();
        for (int k = minDistrictSize; k <= maxDistrictSize; k++) {
            System.out.println("");
            System.out.println("getDistrictsMap voor district met " + k + " units ");

            Set<District> districts = getDistrictsMap(grid, k, peoplePerVoteCell, input.getPeoplePerDistrict());
            if (districts.size() > 0) {
                districtsMap.put(k, districts);
            }
        }

        int minIndex = BoundaryUtils.getMinimumSolutionSize(grid.getSize(), districtsMap.keySet());
        int maxIndex = BoundaryUtils.getMaximumSolutionSize(grid.getSize(), minIndex, districtsMap.keySet());

        List<Solution> solutions = new LinkedList<>();

        for (int k = minIndex; k <= maxIndex; k++) {
            List<Solution> part = getSolutions(grid, districtsMap, k);
            if (part.size() > 0) {
                solutions.addAll(part);
            }
        }

        return solutions;
    }


    private List<Integer> getPossibleUnitSizes(Map<Integer, Set<String>> peoplePerVoteCell) {

        List<Integer> result = new ArrayList<>();

        for (Integer key : peoplePerVoteCell.keySet()) {
            int size = peoplePerVoteCell.get(key).size();
            for (int k = 1; k <= size; k++) {
                result.add(key);
            }
        }
        return result;
    }

    private List<Solution> getSolutions(Grid grid,
                                      Map<Integer, Set<District>> districtsMap,
                                      int countDistricts) {

        System.out.println("");
        System.out.println("Start berekening solutions voor countDistricts " + countDistricts);

        List<Solution> result = new ArrayList<>();

        // divide the grid into districts --> Factor. Each district has a different number of units.
        Set<Factor> d3 = FactorUtils.divideFactors(grid.getSize(), countDistricts, districtsMap.keySet());

        for (Factor factor : d3) {
            System.out.println("Bereken solution voor factor " + factor);
            List<Set<District>> multiList = new ArrayList<>();

            for (Integer elm : factor.getElements()) {
                Set<District> districts = districtsMap.get(elm);
                if (districts != null) {
                    multiList.add(districts);
                }
            }

            Set<Solution> solutions = DistrictUtils.crossProductDistrict(multiList);
            result.addAll(solutions);
        }

        System.out.println("Alle solutions zijn berekend voor countDistricts " + countDistricts);
        System.out.println("");

        return result;
    }

    private Set<District> getDistrictsMap(Grid grid,
                                        int countUnits,
                                        Map<Integer, Set<String>> peoplePerVoteCell,
                                        int peoplePerDistrict) {

        Set<District> result = new HashSet<>();

        // divide one district into units, each district has the same people-count
        Set<Factor> fs = FactorUtils.divideFactors(peoplePerDistrict, countUnits, peoplePerVoteCell.keySet());

        for (Factor factor : fs) {
            List<Set<String>> multiSet = new ArrayList<>();

            for (Integer elm : factor.getElements()) {
                Set<String> sublist = peoplePerVoteCell.get(elm);
                multiSet.add(sublist);
            }

            Set<District> districts = DistrictUtils.crossProductVoteCell(multiSet);

            System.out.println("Aantal district combis voor adjacent filtering " + factor + " --> " + districts.size());
            // each district must contain adjacent units
            districts = GridUtils.filterAdjacentDistricts(grid, districts);
            result.addAll(districts);
        }
        return result;
    }
}
