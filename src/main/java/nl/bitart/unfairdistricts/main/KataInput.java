package nl.bitart.unfairdistricts.main;


import nl.bitart.unfairdistricts.core.Grid;


public class KataInput {

    private final Grid grid;

    private final int peoplePerDistrict;

    public KataInput(Grid grid, int peoplePerDistrict) {
        this.grid = grid;
        this.peoplePerDistrict = peoplePerDistrict;
    }

    public Grid getGrid() {
        return grid;
    }

    public int getPeoplePerDistrict() {
        return peoplePerDistrict;
    }
}
