package nl.bitart.unfairdistricts.utils;


import nl.bitart.unfairdistricts.dto.District;
import nl.bitart.unfairdistricts.dto.Solution;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class DistrictUtils {

    public static Set<District> crossProductVoteCell(List<Set<String>> multiSet) {

        if (multiSet.size() == 0) {
            return new HashSet<>();
        }

        Set<String> units = multiSet.get(0);
        Set<District> combinedSet = convertToDistrict(units);

        for (int k = 1; k < multiSet.size(); k++) {
            Set<String> setExtra = multiSet.get(k);
            combinedSet = getVoteCellCombinations(combinedSet, setExtra);
        }

        return combinedSet;
    }

    public static Set<Solution> crossProductDistrict(List<Set<District>> multiSet) {
        if (multiSet.size() == 0) {
            return new HashSet<>();
        }

        Set<District> units = multiSet.get(0);
        Set<Solution> combinedSet = convertToSolution(units);

        System.out.println("combinedset " + "0" + " - " + combinedSet.size());

        for (int k = 1; k < multiSet.size(); k++) {
            Set<District> setExtra = multiSet.get(k);
            combinedSet = getDistrictCombinations(combinedSet, setExtra);
            System.out.println("combinedset " + k + " - " + combinedSet.size());
        }

        return combinedSet;
    }

    public static Set<District> getVoteCellCombinations(Set<District> set1, Set<String> set2) {

        Set<District> res = new TreeSet<>();

        for (District elm1 : set1) {
            for (String elm2 : set2) {

                if (!elm1.contains(elm2)) {

                    District combi = new District(elm1);
                    combi.combine(elm2);
                    res.add(combi);
                }
            }
        }
        return res;
    }

    public static Set<Solution> getDistrictCombinations(Set<Solution> set1, Set<District> set2) {

        Set<Solution> res = new TreeSet<>();

        for (Solution elm1 : set1) {
            for (District elm2 : set2) {

                if (isDisjunct(elm1,elm2)) {

                    Solution combi = new Solution(elm1);
                    combi.combine(elm2);
                    res.add(combi);
                }
            }
        }
        return res;
    }

    private static boolean isDisjunct(Solution elm1, District elm2) {

        for (District d : elm1.getChildren() ) {

            Set<String> intersection = new HashSet<>(d.getChildren()); // use the copy constructor
            intersection.retainAll(elm2.getChildren());
            if (intersection.size() > 0) {
                return false;
            }
        }
        return true;
    }

    private static Set<District> convertToDistrict(Set<String> units) {

        Set<District> result = new TreeSet<>();
        for (String unit : units) {
            District combi = new District();
            combi.combine(unit);
            result.add(combi);
        }
        return result;
    }

    private static Set<Solution> convertToSolution(Set<District> units) {

        Set<Solution> result = new TreeSet<>();
        for (District unit : units) {
            Solution combi = new Solution();
            combi.combine(unit);
            result.add(combi);
        }
        return result;
    }

    public static Set<String> getNames(District district) {

        Set<String> result = new TreeSet<>();
        for ( String elm : district.getChildren()) {
            result.add(elm);
        }
        return result;
    }
}
