package nl.bitart.unfairdistricts.utils;


import nl.bitart.unfairdistricts.core.Factor;

import java.util.List;
import java.util.Set;


public class BoundaryUtils {

    public static int getMaximumDistrictSize(int peoplePerDistrict, List<Integer> unitSizes) {
        int k = 0;
        int total = 0;

        while (total < peoplePerDistrict) {
            k++;
            total = berekenMin(k, unitSizes);
        }
        return k;
    }

    public static int berekenMin(int size, List<Integer> unitSizes) {
        int total = 0;

        for (int k = 1; k <= size; k++) {
            total = total + unitSizes.get(k - 1);
        }
        return total;
    }

    public static int getMinimumSolutionSize(int totalCount, Set<Integer> keyset) {
        int k = 0;

        boolean bFound = false;
        while (!bFound) {
            k++;
            Set<Factor> factors = FactorUtils.divideFactors(totalCount, k, keyset);
            for (Factor factor : factors) {
                if (isFactorOk(factor, keyset)) {
                    bFound = true;
                    break;
                }
            }
        }
        return k;
    }

    public static int getMaximumSolutionSize(int totalCount, int offset, Set<Integer> keyset) {

        int k = offset;

        boolean found = false;
        while (!found) {
            boolean current = isIndexOk(totalCount, k, keyset);
            boolean next = isIndexOk(totalCount, k + 1, keyset);
            if (current && !next) {
                break;
            }
            k++;
        }
        return k;
    }

    private static boolean isFactorOk(Factor factor, Set<Integer> keyset) {
        for (Integer elm : factor.getElements()) {
            if (!keyset.contains(elm)) {
                return false;
            }
        }
        return true;
    }

    private static boolean isIndexOk(int totalCount, int k, Set<Integer> keyset) {

        Set<Factor> factors = FactorUtils.divideFactors(totalCount, k, keyset);
        for (Factor factor : factors) {
            if (isFactorOk(factor, keyset)) {
                return true;
            }
        }
        return false;
    }
}
