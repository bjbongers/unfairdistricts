package nl.bitart.unfairdistricts.utils;


import nl.bitart.unfairdistricts.core.Factor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;


public class FactorUtils {

    /**
     * bijvoorbeeld dividefactors [8,2, {2,3,4,5}]  -->
     * SKIP        [1,7]
     * SKIP        [2,6]
     * INCLUDE     [3,5]
     * INCLUDE     [4,4]
     * PERMUTATION [7,1]
     * PERMUTATION [6,2]
     * PERMUTATION [5,3]
     * PERMUTATION [4,4]
     */
    public static Set<Factor> divideFactors(int total, int numberOfUnits, Set<Integer> districtSizes) {

        List<List<Integer>> lists = new ArrayList<>();
        for (int k = 1; k <= numberOfUnits; k++) {
            List<Integer> sublist = new LinkedList<>();
            for (int j = 1; j <= total; j++) {
                if (districtSizes.contains(j)) {
                    sublist.add(j);
                }
            }
            lists.add(sublist);
        }
        Set<List<Integer>> combs = getCombinations(lists, total);
        Set<Factor> factors = convertCombinationsToFactors(combs, total);
        return removePermutations(factors);
    }


    public static <T> Set<List<T>> getCombinations(List<List<T>> lists, int total) {
        Set<List<T>> combinations = new HashSet<>();
        Set<List<T>> newCombinations;

        int index = 0;

        // extract each of the integers in the first list and add each to ints as a new list
        for (T i : lists.get(0)) {
            List<T> newList = new ArrayList<>();
            newList.add(i);
            combinations.add(newList);
        }
        index++;
        while (index < lists.size()) {
            List<T> nextList = lists.get(index);
            newCombinations = new HashSet<>();
            for (List<T> first : combinations) {
                for (T second : nextList) {
                    List<T> newList = new ArrayList<>();
                    if (!first.equals(second)) {
                        newList.addAll(first);
                        newList.add(second);
                        newCombinations.add(newList);
                    }
                }
            }
            combinations = newCombinations;

            index++;
        }
        //System.out.println("count combiations for index " + index + ": " + combinations.size());

        return combinations;
    }

    private static Set<Factor> convertCombinationsToFactors(Set<List<Integer>> combs, int total) {

        Set<Factor> result = new TreeSet<>();
        for (List<Integer> comb : combs) {
            Factor factor = new Factor(comb);
            if (factor.getSum() == total) {
                result.add(factor);
            }
        }
        return result;
    }

    private static Set<Factor> removePermutations(Set<Factor> factors) {

        Set<Factor> result = new TreeSet<>();
        Set<Integer> products = new HashSet<>();
        for (Factor factor : factors) {
            int product = factor.getProduct();

            if (!products.contains(product)) {
                products.add(product);
                result.add(factor);
            }
        }
        return result;
    }
}
