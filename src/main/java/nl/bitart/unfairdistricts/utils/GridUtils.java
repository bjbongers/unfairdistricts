package nl.bitart.unfairdistricts.utils;


import nl.bitart.unfairdistricts.core.Grid;
import nl.bitart.unfairdistricts.dto.District;
import nl.bitart.unfairdistricts.dto.Score;
import nl.bitart.unfairdistricts.dto.Solution;
import nl.bitart.unfairdistricts.dto.VoteCell;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;


public class GridUtils {

    public static Map<Integer, Set<String>> getPeoplePerVoteCell(Grid grid) {

        Map<Integer, Set<String>> result = new TreeMap<>();

        for (int i = 0; i < grid.getRowSize(); i++) {
            for (int j = 0; j < grid.getColumnSize(); j++) {
                VoteCell voteCell = grid.getVoteCel(i, j);
                int people = voteCell.getVoteA() + voteCell.getVoteB();
                Set<String> set = result.computeIfAbsent(people, k -> new HashSet<>());
                set.add(voteCell.getDescription());
            }
        }

        return result;
    }

    public static Score getScore(Grid grid, Solution solution) {

        int wins = 0;
        int losses = 0;
        int draws = 0;

        for (District d : solution.getChildren()) {

            int green = 0;
            int red = 0;

            for (VoteCell cell : grid.getVoteCells(d.getName())) {
                if (cell.getVoteA() > cell.getVoteB()) {
                    green++;
                }
                if (cell.getVoteA() < cell.getVoteB()) {
                    red++;
                }
            }

            if (green == red) {
                draws++;
            }
            if (green > red) {
                wins++;
            }
            if (green < red) {
                losses++;
            }
        }

        return new Score(wins, losses, draws);
    }

    public static Set<District> filterAdjacentDistricts(Grid grid, Set<District> districts) {
        Set<District> set = new TreeSet<>();
        for (District elm : districts) {
            if (isAdjacentDistrict(grid, elm)) {
                set.add(elm);
            }
        }
        return set;
    }

    public static boolean isAdjacentDistrict(Grid grid, District district) {
        Set<String> districtUnits = DistrictUtils.getNames(district);
        Set<String> traversed = new HashSet<>();
        String first = district.getChildren().iterator().next();
        getReachableUnits(grid, traversed, districtUnits, first);
        return (traversed.size() == districtUnits.size());
    }

    private static void getReachableUnits(Grid grid, Set<String> traversed, Set<String> districtUnits, String unit) {

        if (districtUnits.contains(unit)) {
            traversed.add(unit);

            Set<String> neighbours = getNeigbours(grid, unit);
            for (String neighbour : neighbours) {
                if (!traversed.contains(neighbour)) {
                    getReachableUnits(grid, traversed, districtUnits, neighbour);
                }
            }
        }
    }

    private static Set<String> getNeigbours(Grid grid, String unit) {

        VoteCell currentCell = grid.getVoteCellByName(unit);
        Set<String> result = new HashSet<>();
        VoteCell left = grid.getLeft(currentCell);
        if (left != null) {
            result.add(left.getDescription());
        }
        VoteCell right = grid.getRight(currentCell);
        if (right != null) {
            result.add(right.getDescription());
        }
        VoteCell up = grid.getUp(currentCell);
        if (up != null) {
            result.add(up.getDescription());
        }
        VoteCell down = grid.getDown(currentCell);
        if (down != null) {
            result.add(down.getDescription());
        }
        return result;
    }

}
