package nl.bitart.unfairdistricts.core;


import java.util.List;


public class Factor implements Comparable {

    private final List<Integer> elements;

    public Factor(List<Integer> elements) {
        this.elements = elements;
    }

    public List<Integer> getElements() {
        return elements;
    }

    public int getSum() {
        return elements.stream().mapToInt(elm -> elm).sum();
    }

    public int getProduct() {
        int prod = 1;
        for (Integer elm : elements) {
            prod = prod * elm;
        }
        return prod;
    }

    @Override
    public String toString() {
        String result = "[";
        int k = 0;
        for (Integer elm : elements) {
            if (k > 0) {
                result = result + ", ";
            }
            k++;
            result = result + elm;
        }
        result = result + "]";
        return result;
    }

    @Override
    public int compareTo(Object o) {
        return this.toString().compareTo(o.toString());
    }
}
