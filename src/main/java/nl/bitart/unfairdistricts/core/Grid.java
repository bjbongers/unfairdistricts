package nl.bitart.unfairdistricts.core;


import nl.bitart.unfairdistricts.dto.VoteCell;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Grid {

    private final VoteCell[][] cells;

    private final int rowSize;
    private final int columnSize;

    private final Map<String, VoteCell> cellMap;

    public Grid(int rowSize, int columnSize) {
        this.rowSize = rowSize;
        this.columnSize = columnSize;
        this.cells = new VoteCell[rowSize][columnSize];
        this.cellMap = new HashMap<>();
    }

    public VoteCell getVoteCel(int row, int column) {
        return cells[row][column];
    }

    public void setVoteCell(VoteCell voteCell) {
        cells[voteCell.getRow()][voteCell.getColumn()] = voteCell;
        cellMap.put(voteCell.getDescription(), voteCell);
    }

    public int getRowSize() {
        return rowSize;
    }

    public int getColumnSize() {
        return columnSize;
    }

    public List<VoteCell> getVoteCells(String cells) {
        String[] split = cells.split("(?!^)");
        return Arrays.stream(split).map(cellMap::get).collect(Collectors.toList());
    }


    public VoteCell getLeft(VoteCell unit) {
        return getCell(unit.getRow(), unit.getColumn() - 1);
    }

    public VoteCell getRight(VoteCell unit) {
        return getCell(unit.getRow(), unit.getColumn() + 1);
    }

    public VoteCell getUp(VoteCell unit) {
        return getCell(unit.getRow() - 1, unit.getColumn());
    }

    public VoteCell getDown(VoteCell unit) {
        return getCell(unit.getRow() + 1, unit.getColumn());
    }

    private VoteCell getCell(int row, int column) {
        if (row < 0 || column < 0 || (row > this.rowSize-1) || (column > this.columnSize-1)) {
            return null;
        }
        return cells[row][column];
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[").append("\n");

        for (int i = 0; i < getRowSize(); i++) {
            for (int j = 0; j < getColumnSize(); j++) {
                sb.append(getVoteCel(i, j).getDistrict());
            }
            sb.append("\n");
        }
        sb.append("]").append("\n");
        return sb.toString();
    }

    public int getSize() {
        return rowSize * columnSize;
    }


    public VoteCell getVoteCellByName(String name) {
        return cellMap.get(name);
    }
}
